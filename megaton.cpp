/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file ev.cpp
///// @version 1.0
/////
///// @author Tomoko Austin <tomokoau@hawaii.edu>
///// @date 16_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "megaton.h"
double fromMegatonToJoule( double megaton ) {
    return megaton / MEGATON_IN_A_JOULE ;
}
double fromJouleToMegaton( double joule ) {
    return joule * MEGATON_IN_A_JOULE ;
}
