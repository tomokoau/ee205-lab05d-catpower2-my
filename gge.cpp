/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file gge.cpp
///// @version 1.0
/////
///// @author Tomoko Austin <tomokoau@hawaii.edu>
///// @date 16_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "gge.h"
double fromGasolineToJoule( double gasoline ){
      return gasoline / GASOLINE_IN_A_JOULE;
}
double fromJouleToGasoline ( double joule ){
      return joule * GASOLINE_IN_A_JOULE;

}
